package com.powersoft.networking;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class Main2Activity extends AppCompatActivity{

    List<DrawerItem> dataList;
    private ListView mDrawerList;
    CustomDrawerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        dataList = new ArrayList<DrawerItem>();
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        dataList.add(new DrawerItem("Message", R.mipmap.ic_launcher));
        dataList.add(new DrawerItem("Likes",R.mipmap.ic_launcher));
        dataList.add(new DrawerItem("Games", R.mipmap.ic_launcher));
        dataList.add(new DrawerItem("Lables", R.mipmap.ic_launcher));
        dataList.add(new DrawerItem("Search",R.mipmap.ic_launcher));
        dataList.add(new DrawerItem("Cloud", R.mipmap.ic_launcher));
        dataList.add(new DrawerItem("Camara", R.mipmap.ic_launcher));
        dataList.add(new DrawerItem("Video", R.mipmap.ic_launcher));
        dataList.add(new DrawerItem("Groups", R.mipmap.ic_launcher));
        dataList.add(new DrawerItem("Import & Export", R.mipmap.ic_launcher));
        dataList.add(new DrawerItem("About", R.mipmap.ic_launcher));
        dataList.add(new DrawerItem("Settings", R.mipmap.ic_launcher));
        dataList.add(new DrawerItem("Help", R.mipmap.ic_launcher));

        adapter = new CustomDrawerAdapter(this, R.layout.custom_drawer_item,
                dataList);
        mDrawerList.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
}
