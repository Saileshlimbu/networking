package com.powersoft.networking;

import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;

    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    CustomDrawerAdapter adapter;

    List<DrawerItem> dataList;
    private ListView mDrawerList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        // Initializing
        dataList = new ArrayList<DrawerItem>();
        mTitle = mDrawerTitle = getTitle();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        dataList.add(new DrawerItem("Message", R.mipmap.ic_launcher));
        dataList.add(new DrawerItem("Likes",R.mipmap.ic_launcher));
        dataList.add(new DrawerItem("Games", R.mipmap.ic_launcher));
        dataList.add(new DrawerItem("Lables", R.mipmap.ic_launcher));
        dataList.add(new DrawerItem("Search",R.mipmap.ic_launcher));
        dataList.add(new DrawerItem("Cloud", R.mipmap.ic_launcher));
        dataList.add(new DrawerItem("Camara", R.mipmap.ic_launcher));
        dataList.add(new DrawerItem("Video", R.mipmap.ic_launcher));
        dataList.add(new DrawerItem("Groups", R.mipmap.ic_launcher));
        dataList.add(new DrawerItem("Import & Export", R.mipmap.ic_launcher));
        dataList.add(new DrawerItem("About", R.mipmap.ic_launcher));
        dataList.add(new DrawerItem("Settings", R.mipmap.ic_launcher));
        dataList.add(new DrawerItem("Help", R.mipmap.ic_launcher));

        adapter = new CustomDrawerAdapter(this, R.layout.custom_drawer_item,
                dataList);
        mDrawerList.setAdapter(adapter);

    }
}
